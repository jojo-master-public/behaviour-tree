### Node Based Behaviour Tree

This project is focused on implementing a node based Behaviour Tree in Unity using GraphView (the node editor base that is used in Shader Graph).
The project is still Work In Progress and will take some time due to other projects taking up priorities.

Currently it looks like this

![Node Editor](/screenshots/screenshot_0.png)