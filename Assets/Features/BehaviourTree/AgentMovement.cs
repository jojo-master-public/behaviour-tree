using UnityEngine;
using UnityEngine.AI;

namespace Project.AI.BehaviourTree.Agents
{
    public class AgentMovement : MonoBehaviour
    {
        [SerializeField] private float _runMultiplier = 1.25f;
        [SerializeField] private float _doorCheckDistance;
        [SerializeField] private float _doorOpenDuration;
        [SerializeField] private LayerMask _doorMask;
        [SerializeField] private Vector3 _centerOffset;

        private NavMeshAgent _agent;

        private float _agentMovementSpeed;



        public void MoveTo(Vector3 target, bool isRunning = false)
        {
            if (HorizontalDistance(target, _agent.destination) < _agent.radius)
                return;

            _agent.speed = _agentMovementSpeed * (isRunning ? _runMultiplier : 1f);
            _agent.enabled = true;
            _agent.SetDestination(target);
            _agent.isStopped = false;
        }

        public void Stop()
        {
            if (_agent.enabled)
            {
                _agent.isStopped = true;
                _agent.enabled = false;
            }
        }

        private void Awake()
        {
            _agent = GetComponent<NavMeshAgent>();

            _agentMovementSpeed = _agent.speed;
        }

        public float HorizontalDistance(Vector3 a, Vector3 b)
        {
            a.y = b.y = 0f;

            return Vector3.Distance(a, b);
        }
    }
}