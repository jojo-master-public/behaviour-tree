using UnityEngine;
using UnityEngine.Playables;
using System.Linq;
using Dioinecail.ButtonUtil;
using System.Collections.Generic;

namespace Project.AI.Utilities
{
    [System.Serializable]
    public class TimelineMapping
    {
        [HideInNormalInspector]
        public string Id;
        public PlayableDirector Target;
    }

    public class TimelineMappingComparer : IComparer<TimelineMapping>
    {
        public int Compare(TimelineMapping x, TimelineMapping y)
        {
            return x.Id.CompareTo(y.Id);
        }
    }

    public class TimelineMappings : MonoBehaviour
    {
        [SerializeField] private List<TimelineMapping> m_Mappings;

#if UNITY_EDITOR

        public List<TimelineMapping> Mappings => m_Mappings;

#endif



        public PlayableDirector GetTimeline(string id)
        {
            var mapping = m_Mappings.FirstOrDefault(m => m.Id.Equals(id));

            if (mapping == null)
                Debug.LogError($"[{nameof(TimelineMappings)}.GetTimeline({id})] Could not find a PlayableDirector with id:'{id}'!");

            return mapping == null ? null : mapping.Target;
        }

        [Button]
        private void FindTimelines()
        {
            if (m_Mappings == null)
                m_Mappings = new List<TimelineMapping>();

            var timelines = FindObjectsOfType<PlayableDirector>(true);

            foreach (var t in timelines)
            {
                if (!m_Mappings.Any(m => m.Target.Equals(t)))
                {
                    m_Mappings.Add(new TimelineMapping
                    {
                        Id = t.name,
                        Target = t
                    });
                }
            }

            m_Mappings.Sort(new TimelineMappingComparer());
        }
    }
}