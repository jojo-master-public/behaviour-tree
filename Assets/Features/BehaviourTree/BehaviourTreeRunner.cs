using System;
using UnityEngine;
using Project.AI.BehaviourTree.Agents;
using Project.AI.BehaviourTree.Nodes;

namespace Project.AI.BehaviourTree
{
    public class BehaviourTreeRunner : MonoBehaviour
    {
        public event Action<BehaviourTreeRunner> onTreeStarted;

        [SerializeField] private bool m_RepeatOnFinished;

        private BehaviourAgent m_Agent;
        private BehaviourTree m_TargetTree;

        public BehaviourTree CurrentTree { get; private set; }



        public void Play(BehaviourTree tree)
        {
            if(CurrentTree != null)
            {
                Destroy(CurrentTree);
                CurrentTree = null;
            }

            m_TargetTree = tree;

            CurrentTree = m_TargetTree.Clone();
            CurrentTree.Play(m_Agent);

            onTreeStarted?.Invoke(this);
        }

        public void Play(BehaviourTree tree, BehaviourAgent agent)
        {
            m_Agent = agent;

            Play(tree);
        }

        private void Awake()
        {
            m_Agent = GetComponent<BehaviourAgent>();
        }

        private void Update()
        {
            var state = CurrentTree?.Update();

            if (m_RepeatOnFinished)
            {
                if(state == Node.State.Failure || state == Node.State.Success)
                {
                    Play(m_TargetTree);
                }
            }
        }

        private void OnDisable()
        {
            if(CurrentTree != null)
            {
                Destroy(CurrentTree);
                CurrentTree = null;
            }
        }
    }
}