using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    public enum PositionMode
    {
        Fixed,
        ReadFromAgent
    }

    [NodeInfo("execute", "Move To Position", "Movement/Move To Position")]
    public class MoveToPositionNode : MoveToNodeBase
    {
        [SerializeField, DisplayOnNode] private PositionMode m_PositionMode;
        [SerializeField, DisplayOnNode] private Vector3 m_TargetPosition;



        protected override void GetTarget()
        {
            if(m_PositionMode == PositionMode.ReadFromAgent)
                Tree.Agent.ReadData(NodesDataIds.TARGET_POSITION, out m_TargetPosition);
        }

        protected override bool TargetCheck()
        {
            if (m_PositionMode == PositionMode.ReadFromAgent)
                return !Tree.Agent.ReadData<Vector3>(NodesDataIds.TARGET_POSITION, out var temp);

            return true;
        }

        protected override Vector3 GetPosition()
        {
            return m_TargetPosition;
        }
    }
}