using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    public enum TargetMode
    {
        DIRECT_TO_TARGET,
        AROUND_THE_TARGET
    }

    [NodeInfo("execute", "MoveToSutablePosition", "Movement/MoveToSutablePosition")]
    public class MoveToSuitablePositionNode : MoveToNodeBase
    {
        [SerializeField, DisplayOnNode] private TargetMode m_TargetMode;
        [SerializeField, DisplayOnNode] private float m_AroundRadius;
        [SerializeField, DisplayOnNode] private float m_AttemptDuration;

        [SerializeField, DisplayOnNode, ReadOnly] private Vector3 m_RandomOffset;
        [SerializeField, DisplayOnNode, ReadOnly] private float m_InitialTime;
        [SerializeField, DisplayOnNode, ReadOnly] private Vector3 m_TargetPosition;



        protected override Vector3 GetPosition()
        {
            switch (m_TargetMode)
            {
                case TargetMode.AROUND_THE_TARGET:
                    return m_TargetPosition + m_RandomOffset;
                default:
                    return m_TargetPosition;
            }
        }

        protected override void GetTarget()
        {
            m_Tree.Agent.ReadData(NodesDataIds.TARGET_POSITION, out m_TargetPosition);
        }

        protected override void OnStartInternal()
        {
            base.OnStartInternal();
            m_RandomOffset = Quaternion.Euler(0, 0, Random.value * 360f) * Vector3.up * m_AroundRadius;
            m_InitialTime = Time.time;
        }

        protected override State OnUpdateInternal()
        {
            var currentState = base.OnUpdateInternal();

            if (m_AttemptDuration > 0f 
                && Time.time - m_InitialTime > m_AttemptDuration)
            {
                if(m_StateOnFail == State.Success && (m_StopOnSuccess))
                    m_Tree.Agent.AgentMovement.Stop();

                return m_StateOnFail;
            }

            return currentState;
        }

        protected override bool TargetCheck()
        {
            return m_Tree.Agent.ReadData(NodesDataIds.TARGET_POSITION, out Vector3 target);
        }
    }
}