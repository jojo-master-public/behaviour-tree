using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    public abstract class MoveToNodeBase : ActionNode
    {
        [SerializeField, DisplayOnNode] protected float m_ApproachThreshold = 0.2f;
        [SerializeField, DisplayOnNode] protected State m_StateOnFail = State.Failure;
        [SerializeField, DisplayOnNode] protected bool m_StopOnSuccess = true;
        [SerializeField, DisplayOnNode] protected bool m_IsRunning = false;



        protected abstract void GetTarget();
        protected abstract bool TargetCheck();
        protected abstract Vector3 GetPosition();

        protected override void OnStartInternal()
        {
            base.OnStartInternal();

            GetTarget();
        }

        protected override State OnUpdateInternal()
        {
            var agent = Tree.Agent;
            var movementBehaviour = agent.AgentMovement;

            if (!TargetCheck())
            {
                return State.Failure;
            }

            var position = GetPosition();

            movementBehaviour.MoveTo(position, m_IsRunning);

            if (movementBehaviour.HorizontalDistance(agent.Position, position) > m_ApproachThreshold)
                return State.Running;
            else
            {
                if (m_StopOnSuccess)
                {
                    movementBehaviour.Stop();
                }

                return State.Success;
            }
        }
    }
}