using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    public enum TargetMode
    {
        DIRECT_TO_TARGET,
        AROUND_THE_TARGET
    }

    [NodeInfo("execute", "MoveToSutablePosition", "Movement/MoveToSutablePosition")]
    public class MoveToSutablePositionNode : ActionNode
    {
        private const string SuitablePositionKey = "suitablePosition";
        private const string ApproachDistanceKey = "approachDistance";
        private const string UseApproachDistanceKey = "useApproachDistance";

        [SerializeField] private TargetMode m_TargetMode;
        [SerializeField] private float m_AroundRadius;
        [SerializeField] private float m_ApproachThreshold;
        [SerializeField] private float m_AttemptDuration;
        [SerializeField] private State m_StateOnFail;
        [SerializeField] private bool m_StopOnSuccess;

        private Vector3 m_RandomOffset;
        private float m_InitialTime;



        protected override void OnStartInternal()
        {
            base.OnStartInternal();
            m_RandomOffset = Quaternion.Euler(0, Random.value * 360f, 0) * Vector3.forward * m_AroundRadius;
            m_InitialTime = Time.time;
        }

        protected override State OnUpdateInternal()
        {
            var agent = Tree.Agent;
            var movementBehaviour = agent.Movement;

            if (!agent.ReadData(SuitablePositionKey, out Vector3 target))
                return State.Failure;

            agent.ReadData(UseApproachDistanceKey, out bool useApproachDistance);

            var approachDistance = m_ApproachThreshold;

            if (useApproachDistance)
            {
                if (!agent.ReadData(ApproachDistanceKey, out float approachDistFromAgent))
                    return State.Failure;
                else
                    approachDistance = approachDistFromAgent;
            }

            Vector3 targetPosition = Vector3.zero;

            switch (m_TargetMode)
            {
                case TargetMode.DIRECT_TO_TARGET:
                    targetPosition = target;
                    break;
                case TargetMode.AROUND_THE_TARGET:
                    targetPosition = target + m_RandomOffset;
                    break;
                default:
                    break;
            }

            movementBehaviour.MoveTo(targetPosition);

            if (Time.time - m_InitialTime > m_AttemptDuration)
            {
                if(m_StateOnFail == State.Success && (m_StopOnSuccess))
                    movementBehaviour.Stop();

                return m_StateOnFail;
            }

            if (Vector3.Distance(agent.Position, targetPosition) > approachDistance)
                return State.Running;
            else
            {
                if(m_StopOnSuccess)
                {
                    movementBehaviour.Stop();
                }

                return State.Success;
            }
        }
    }
}