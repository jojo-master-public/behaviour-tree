using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("execute", "Attack Target", "Combat/Attack Target")]
    public class AttackTargetNode : ActionNode
    {
        protected override void OnStartInternal()
        {
            base.OnStartInternal();

            var agent = Tree.Agent;
            var attackBehaviour = agent.Attack;

            attackBehaviour.Reset();
        }

        protected override State OnUpdateInternal()
        {
            var agent = Tree.Agent;
            var attackBehaviour = agent.Attack;

            if (!agent.ReadData("target", out Transform target))
                return State.Failure;

            var attackState = attackBehaviour.Execute(target);

            switch (attackState)
            {
                case Enemies.Behaviours.AttackState.EXECUTING:
                    return State.Running;
                default:
                    return State.Success;
            }
        }

        protected override void OnStopInternal()
        {
            base.OnStopInternal();

            var agent = Tree.Agent;
            var attackBehaviour = agent.Attack;

            attackBehaviour.Reset();
        }
    }
}