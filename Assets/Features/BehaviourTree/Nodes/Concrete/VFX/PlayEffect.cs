using UnityEngine;
using Dioinecail.ServiceLocator;
using Project.Effects;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("execute", "Play Effect", "VFX/Play Effect")]
    public class PlayEffect : ActionNode
    {
        [SerializeField, DisplayOnNode] private EffectBase m_TargetEffect;
        [SerializeField, DisplayOnNode] private bool m_Self = true;



        protected override State OnUpdateInternal()
        {
            var effectsManager = ServiceLocator.Get<IEffectsManager>();

            if (effectsManager == null)
                return State.Failure;

            var transform = Tree.Agent.transform;

            if(m_Self)
                effectsManager.SpawnEffect(m_TargetEffect, transform.position, transform.rotation);
            else
            {
                // TODO: decide where to spawn the effect
            }

            return State.Success;
        }
    }
}