using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("execute", "PlayTree", "Logic/Play Tree")]
    public class PlayTreeNode : ActionNode
    {
        [SerializeField, DisplayOnNode] private BehaviourTree m_Target;



        protected override State OnUpdateInternal()
        {
            Tree.Agent.TreeRunner.Play(m_Target);

            return State.Success;
        }
    }
}