using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("decorator", "Alter", "Logic/Alter", true, true)]
    public class AlterNode : DecoratorNode
    {
        private enum ReturnMode
        {
            Fixed,
            Invert,
            Random
        }

        [SerializeField, DisplayOnNode] private ReturnMode m_ReturnMode = ReturnMode.Fixed;
        [SerializeField, DisplayOnNode] private State m_FixedState = State.Success;



        protected override State OnUpdateInternal()
        {
            var childState = base.OnUpdateInternal();

            switch (childState)
            {
                case State.Success:
                case State.Failure:
                    switch (m_ReturnMode)
                    {
                        default:
                        case ReturnMode.Fixed:
                            return m_FixedState;
                        case ReturnMode.Invert:
                            if (childState == State.Success)
                                return State.Failure;

                            return State.Success;

                        case ReturnMode.Random:
                            return UnityEngine.Random.value > 0.5f
                                ? State.Success
                                : State.Failure;
                    }

                default:
                    return childState;
            }
        }
    }
}