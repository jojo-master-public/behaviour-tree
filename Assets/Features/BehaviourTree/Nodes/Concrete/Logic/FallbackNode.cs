using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("composite", "Fallback", "Logic/Fallback")]
    public class FallbackNode : CompositeNode
    {
#if UNITY_EDITOR
        [SerializeField, ReadOnly, TextArea] private string m_Description = "���� ������ ���� ��������, �� �������� ����� �� ������ ����. ������ � �������� \"Fallback\"";
#endif


        protected override State OnUpdateInternal()
        {
            int nodeIndex = 0;

            var nodeIsNull = ExecutionPorts[nodeIndex].output.index < 0;

            if (nodeIsNull)
            {
                return State.Failure;
            }

            var guidFirstSlot = ExecutionPorts[0].output.guid;
            var childFirstSlot = Tree.GetNodeByGuid(guidFirstSlot);

            var stateFirstSlot = childFirstSlot.Update();

            if(stateFirstSlot == State.Failure)
            {
                var guidSecondSlot = ExecutionPorts[1].output.guid;
                var childSecondSlot = Tree.GetNodeByGuid(guidSecondSlot);

                return childSecondSlot.Update();
            }

            return stateFirstSlot;
        }

        protected override void InitOutputs()
        {
            if (ExecutionPorts.Count != 2)
            {
                ExecutionPorts = new System.Collections.Generic.List<NodePort>();
                ExecutionPorts.Add(new NodePort());
                ExecutionPorts.Add(new NodePort());
            }
        }
    }
}
