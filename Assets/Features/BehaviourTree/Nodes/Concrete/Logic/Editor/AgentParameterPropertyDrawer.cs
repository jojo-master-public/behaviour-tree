using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Project.AI.BehaviourTree.Agents;
using System.Linq;
using System;
using System.Reflection;

namespace Project.AI.BehaviourTree.Nodes.Editors
{
    [CustomPropertyDrawer(typeof(AgentParameter))]
    public class AgentParameterPropertyDrawer : DropDownPropertyDrawer
    {
        private const System.Reflection.BindingFlags m_FieldMask = System.Reflection.BindingFlags.Instance | System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Public;
        protected override string m_PropertyName => "Name";
        private string m_ValueName = "Value";
        protected override PropertyDisplayType m_DisplayType => PropertyDisplayType.Labeled;



        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var idProp = property.FindPropertyRelative(m_PropertyName);
            var displayPosition = position;

            if (m_DisplayType == PropertyDisplayType.Labeled)
            {
                displayPosition = EditorGUI.PrefixLabel(position, new GUIContent(label));
                displayPosition.height = 20f;
            }

            if (EditorGUI.DropdownButton(displayPosition, new GUIContent(idProp.stringValue), FocusType.Keyboard))
            {
                DisplayItemsMenu(idProp);
            }

            var valuePosition = position;
            valuePosition.y += 20f;
            valuePosition.height = 20f;

            OnValueGUI(valuePosition, property, new GUIContent("Value"));
        }

        private void OnValueGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var idProp = property.FindPropertyRelative(m_PropertyName);
            var displayPosition = position;
            displayPosition.height = 20f;

            var parentObj = property.serializedObject.targetObject;
            var parentField = typeof(SetParameterNode).GetField("m_TargetParemeter", m_FieldMask);

            var valueField = typeof(BehaviourAgent).GetField(idProp.stringValue, m_FieldMask);
            var agentParameter = parentField.GetValue(parentObj);

            var agentParameterField = typeof(AgentParameter).GetField("Value", m_FieldMask);
            var agentParameterValue = agentParameterField.GetValue(agentParameter);

            displayPosition = EditorGUI.PrefixLabel(position, new GUIContent(label));

            DisplayFieldValue(displayPosition, agentParameterField, agentParameter, agentParameterValue, valueField.FieldType.ToString());
        }

        protected override IEnumerable<string> LoadItemIds(SerializedProperty property)
        {
            var fields = typeof(BehaviourAgent).GetFields(m_FieldMask);

            return fields.Select(f => f.Name);

            var serializedObject = property.serializedObject;
            var agent = serializedObject.FindProperty("m_Tree").FindPropertyRelative("m_Agent");
        }

        protected override void DisplayItemsMenu(SerializedProperty property)
        {
            var items = LoadItemIds(property);

            GenericMenu menu = new GenericMenu();

            var currentId = property.stringValue;

            {
                var itemIdArg = new IdArgument()
                {
                    Property = property,
                    Id = string.Empty
                };

                menu.AddItem(new GUIContent("Empty String"), string.Equals(currentId, string.Empty), OnItemSelected, itemIdArg);
            }

            foreach (var item in items)
            {
                var itemIdArg = new IdArgument()
                {
                    Property = property,
                    Id = item
                };

                menu.AddItem(new GUIContent(item), string.Equals(currentId, item), OnItemSelected, itemIdArg);
            }

            menu.ShowAsContext();
        }

        protected override void OnItemSelected(object item)
        {
            if (item == null)
                return;

            var itemIdProp = (IdArgument)item;

            itemIdProp.Property.stringValue = itemIdProp.Id;
            itemIdProp.Property.serializedObject.ApplyModifiedProperties();
        }

        private void DisplayFieldValue(Rect position, FieldInfo valueInfo, object parentField, object oldValue, string type)
        {
            if(string.Equals(type, typeof(float).ToString()))
            {
                float newValue = 0f;

                if (oldValue == null || oldValue.GetType().ToString() != type.ToString())
                    newValue = EditorGUI.FloatField(position, 0f);
                else
                    newValue = EditorGUI.FloatField(position, (float)oldValue);

                valueInfo.SetValue(parentField, newValue);
            }

            if (string.Equals(type, typeof(Int32).ToString()))
            {
                int newValue = 0;

                if (oldValue == null || oldValue.GetType().ToString() != type.ToString())
                    newValue = EditorGUI.IntField(position, 0);
                else
                    newValue = EditorGUI.IntField(position, (int)oldValue);

                valueInfo.SetValue(parentField, newValue);
            }

            if (string.Equals(type, typeof(bool).ToString()))
            {
                bool newValue;

                if(oldValue == null || oldValue.GetType().ToString() != type.ToString())                
                    newValue = EditorGUI.Toggle(position, false);
                else
                    newValue = EditorGUI.Toggle(position, (bool)oldValue);

                valueInfo.SetValue(parentField, newValue);
            }

            if(string.Equals(type, typeof(BehaviourTree).ToString()))
            {
                BehaviourTree newValue = null;

                if (oldValue == null || oldValue.GetType().ToString() != type.ToString())
                    newValue = (BehaviourTree)EditorGUI.ObjectField(position, new GUIContent(string.Empty), default(BehaviourTree), typeof(BehaviourTree), false);
                else
                    newValue = (BehaviourTree)EditorGUI.ObjectField(position, new GUIContent(string.Empty), (BehaviourTree)oldValue, typeof(BehaviourTree), false);

                valueInfo.SetValue(parentField, newValue);
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) + 20f;
        }
    }
}