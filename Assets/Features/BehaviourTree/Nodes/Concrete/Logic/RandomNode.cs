using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("composite", "Random", "Logic/Random")]
    public class RandomNode : CompositeNode
    {
        [SerializeField] private string m_Seed;

        private int m_CurrentIndex;
        private System.Random m_Randomizer;


        protected override void OnStartInternal()
        {
            base.OnStartInternal();

            if (m_Randomizer == null)
                m_Randomizer = GetRandomizer(m_Seed);

            m_CurrentIndex = GetRandomIndex();
        }

        protected override State OnUpdateInternal()
        {
            var nodeIsNull = ExecutionPorts[m_CurrentIndex].output.index < 0;

            if (nodeIsNull)
            {
                return State.Failure;
            }

            var guid = ExecutionPorts[m_CurrentIndex].output.guid;
            var child = Tree.GetNodeByGuid(guid);

            return child.Update();
        }

        private int GetRandomIndex()
        {
            return m_Randomizer.Next(0, ExecutionPorts.Count);
        }

        private System.Random GetRandomizer(string seed)
        {
            return new System.Random(seed.GetHashCode());
        }
    }
}