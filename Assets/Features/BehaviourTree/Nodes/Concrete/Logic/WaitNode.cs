using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("execute", "Wait", "Logic/Wait", true, false)]
    public class WaitNode : ActionNode
    {
        [SerializeField, DisplayOnNode] private float m_Duration;
        [SerializeField, Range(0, 1f), DisplayOnNode] private float m_DurationVariance;

        private float m_InitialTime;
        private float m_FinalDuration;



        protected override void OnStartInternal()
        {
            m_InitialTime = Time.time;
            m_FinalDuration = m_Duration + (m_Duration * ((Random.value - 0.5f) * 2f) * m_DurationVariance);
        }

        protected override State OnUpdateInternal()
        {
            var timer = Time.time - m_InitialTime;

            if (timer > m_FinalDuration)
                return State.Success;
            else
                return State.Running;
        }
    }
}