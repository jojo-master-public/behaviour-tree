using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("composite", "Branch", "Logic/Branch")]
    public class BranchNode : CompositeNode
    {
        [SerializeField] private string m_Condition;
        [SerializeField] private bool m_Invert;



        protected override State OnUpdateInternal()
        {
            Tree.Agent.ReadData<bool>(m_Condition, out var data);

            if (m_Invert)
                data = !data;

            int nodeIndex = data ? 0 : 1;

            var nodeIsNull = ExecutionPorts[nodeIndex].output.index < 0;

            if (nodeIsNull)
            {
                return State.Failure;
            }

            var guid = ExecutionPorts[nodeIndex].output.guid;
            var child = Tree.GetNodeByGuid(guid);

            return child.Update();
        }

        protected override void InitOutputs()
        {
            if (ExecutionPorts.Count != 2)
            {
                ExecutionPorts = new System.Collections.Generic.List<NodePort>();
                ExecutionPorts.Add(new NodePort());
                ExecutionPorts.Add(new NodePort());
            }
        }
    }
}