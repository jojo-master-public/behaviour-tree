using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("composite", "Sequence", "Logic/Sequence")]
    public class SequencerNode : CompositeNode
    {
        [SerializeField] private bool m_ContinueOnFail;

        private int CurrentIndex = 0;



        protected override void OnStartInternal()
        {
            base.OnStartInternal();

            CurrentIndex = 0;
        }

        protected override State OnUpdateInternal()
        {
            if (CurrentIndex > ExecutionPorts.Count - 1)
                return State.Success;

            var nodeIsNull = ExecutionPorts[CurrentIndex].output.index < 0;

            if(nodeIsNull)
            {
                CurrentIndex++;
                return State.Running;
            }

            var guid = ExecutionPorts[CurrentIndex].output.guid;
            var child = Tree.GetNodeByGuid(guid);

            State childState = State.Success;

            if(child != null)
                childState = child.Update();

            switch (childState)
            {
                case State.Success:
                    CurrentIndex++;
                    return State.Running;
                case State.Failure:
                    if (m_ContinueOnFail)
                    {
                        CurrentIndex++;
                        return State.Running;
                    }
                    return childState;
                default:
                    break;
            }

            if (childState == State.Success)
            {
                CurrentIndex++;
                return State.Running;
            }

            return childState;
        }
    }
}