using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [System.Serializable]
    public class AgentParameter
    {
        public string Name;
        public object Value;
    }

    [NodeInfo("execute", "Set Parameter", "Actions/Set Parameter")]
    public class SetParameterNode : ActionNode
    {
        [SerializeField] private AgentParameter m_TargetParemeter;



        protected override State OnUpdateInternal()
        {
            throw new System.NotImplementedException();
        }
    }
}