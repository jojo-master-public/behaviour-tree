using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    public enum ParallelType
    {
        ALL,
        ANY
    }

    [NodeInfo("composite", "Parallel", "Logic/Parallel")]
    public class ParallelNode : CompositeNode
    {
        [SerializeField] private ParallelType m_Type = ParallelType.ALL;

        private State[] m_ChildStates;


        protected override void OnStartInternal()
        {
            base.OnStartInternal();

            m_ChildStates = new State[ExecutionPorts.Count];
        }

        protected override State OnUpdateInternal()
        {
            for (int i = 0; i < ExecutionPorts.Count; i++)
            {
                var node = Tree.GetNodeByGuid(ExecutionPorts[i].output.guid);

                if (node != null)
                {
                    m_ChildStates[i] = node.Update();
                }
            }

            if (m_Type == ParallelType.ANY)
            {
                foreach (var state in m_ChildStates)
                {
                    if (state == State.Success || state == State.Failure)
                    {
                        return state;
                    }
                }
            }
            else
            {
                foreach (var state in m_ChildStates)
                {
                    switch (state)
                    {
                        case State.Running:
                            return State.Running;
                        case State.Success:
                            continue;
                        case State.Failure:
                            return State.Failure;
                    }
                }

                return State.Success;
            }

            return State.Running;
        }
    }
}