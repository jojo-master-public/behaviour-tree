namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("execute", "Return To Pool", "Logic/Return to pool")]
    public class ReturnToPool : ActionNode
    {
        protected override State OnUpdateInternal()
        {
            if (Tree.Agent.Enemy == null)
                return State.Failure;

            Tree.Agent.Enemy.ReturnToPool();

            return State.Success;
        }
    }
}