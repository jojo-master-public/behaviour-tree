using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    public enum RepeatMode
    {
        COUNT,
        INFINITE
    }

    [NodeInfo("decorator", "Repeat", "Logic/Repeat")]
    public class RepeatNode : DecoratorNode
    {
        [SerializeField] private RepeatMode m_Mode;
        [SerializeField] private int m_Count;
        [SerializeField] private State m_StateOnFailure;

        private int m_InitialCount = 0;



        protected override void OnStartInternal()
        {
            base.OnStartInternal();

            m_InitialCount = 0;
        }

        protected override State OnUpdateInternal()
        {
            var childState = base.OnUpdateInternal();

            switch (childState)
            {
                case State.Success:
                    switch (m_Mode)
                    {
                        case RepeatMode.COUNT:
                            m_InitialCount++;

                            ResetChild();

                            if(m_InitialCount == m_Count)
                                return State.Success;
                            else 
                                return State.Running;

                        case RepeatMode.INFINITE:
                            ResetChild();
                            return State.Running;
                        default:
                            return State.Running;
                    }
                case State.Failure:
                    return m_StateOnFailure;
                default:
                    return State.Running;
            }
        }
    }
}