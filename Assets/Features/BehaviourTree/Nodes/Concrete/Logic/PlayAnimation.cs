using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("execute", "Play Animation", "Logic/Play Animation")]
    public class PlayAnimation : ActionNode
    {
        [SerializeField, DisplayOnNode] private string m_AnimationName;
        [SerializeField, DisplayOnNode] private int m_LayerIndex;



        protected override State OnUpdateInternal()
        {
            if (Tree.Agent.Animator == null)
                return State.Failure;

            Tree.Agent.Animator.Play(m_AnimationName, m_LayerIndex);

            return State.Success;
        }
    }
}