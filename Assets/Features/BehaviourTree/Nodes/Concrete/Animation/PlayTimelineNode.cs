using Project.AI.Utilities;
using UnityEngine;
using UnityEngine.Playables;

namespace Project.AI.BehaviourTree.Nodes
{
    [System.Serializable]
    public class Timeline
    {
        public string Id;
    }

    [NodeInfo("execute", "Play Timeline", "Animation/Play Timeline")]
    public class PlayTimelineNode : ActionNode
    {
        // ���� ���������� �������, ���� ��� ���� ����� �������������� ���� ������������ ��������..
        // ��� ������� ������� ����� _timeline ��� ����� ��� ������� TimelineId � ������� �� �����, �� ����� ���������� ����� ���������������� ������ ���������

        [SerializeField, DisplayOnNode] private Timeline _timeline;
        [SerializeField, DisplayOnNode] private bool _waitForTimeline;

        [SerializeField, ReadOnly, DisplayOnNode] private bool _isPlaying;
        [SerializeField, ReadOnly, DisplayOnNode] private PlayableDirector _timelineReference;



        protected override void OnStartInternal()
        {
            base.OnStartInternal();

            FindTimeline();

            _timelineReference.Play();

            if (_waitForTimeline)
            {
                _isPlaying = true;
                _timelineReference.stopped += HandleTimelineStopped;
            }
        }

        protected override State OnUpdateInternal()
        {
            if (_timelineReference == null)
                return State.Failure;

            if (_isPlaying)
                return State.Running;

            return State.Success;
        }

        private void HandleTimelineStopped(PlayableDirector director)
        {
            director.stopped -= HandleTimelineStopped;

            _isPlaying = false;
        }

        private void FindTimeline()
        {
            var mappingsObject = FindObjectOfType<TimelineMappings>();

            if (mappingsObject == null)
                return;

            _timelineReference = mappingsObject.GetTimeline(_timeline.Id);   
        }
    }
}