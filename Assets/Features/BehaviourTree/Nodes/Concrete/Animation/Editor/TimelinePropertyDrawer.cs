using Project.AI.Utilities;
using UnityEngine;
using UnityEngine.Playables;
using UnityEditor;
using System.Collections.Generic;
using System.Linq;

namespace Project.AI.BehaviourTree.Nodes.Editors
{
    [CustomPropertyDrawer(typeof(Timeline))]
    public class TimelinePropertyDrawer : DropDownPropertyDrawer
    {
        protected override string m_PropertyName => "Id";
        protected override PropertyDisplayType m_DisplayType => PropertyDisplayType.Labeled;

        private IEnumerable<string> m_CachedIds;



        protected override IEnumerable<string> LoadItemIds(SerializedProperty property)
        {
            if (m_CachedIds == null)
            {
                var mappingObject = Object.FindObjectOfType<TimelineMappings>();

                if(mappingObject == null)
                {
                    Debug.LogError($"[{nameof(TimelinePropertyDrawer)}.{nameof(LoadItemIds)}] Could not find mappings object");

                    return null;
                }

                m_CachedIds = mappingObject.Mappings.Select(m => m.Id);
            }

            return m_CachedIds;
        }
    }
}