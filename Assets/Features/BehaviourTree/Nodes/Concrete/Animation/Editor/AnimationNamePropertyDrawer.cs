using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Animations;
using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes.Editors
{
    public class AnimationStateComparer : IComparer<ChildAnimatorState>
    {
        public int Compare(ChildAnimatorState x, ChildAnimatorState y)
        {
            return x.state.name.CompareTo(y.state.name);
        }
    }

    [CustomPropertyDrawer(typeof(AnimationName))]
    public class AnimationNamePropertyDrawer : DropDownPropertyDrawer
    {
        protected override string m_PropertyName => "Name";
        protected override PropertyDisplayType m_DisplayType => PropertyDisplayType.Labeled;

        private Dictionary<string, float> m_CachedDurations; 
        private IEnumerable<string> m_CachedIds;



        protected override IEnumerable<string> LoadItemIds(SerializedProperty property)
        {
            if (m_CachedIds == null)
            {
                try
                {
                    m_CachedDurations = new Dictionary<string, float>();

                    var agent = UnityEngine.Object.FindObjectOfType<Agents.BehaviourAgent>();
                    var animator = (AnimatorController)agent.GetComponent<Animator>().runtimeAnimatorController;

                    var states = animator.layers[0].stateMachine.states;

                    Array.Sort(states, new AnimationStateComparer());

                    m_CachedIds = states.Select(c => c.state.name);

                    foreach (var childAnimatorState in states)
                    {
                        var stateName = childAnimatorState.state.name;
                        var stateMotion = (AnimationClip)childAnimatorState.state.motion;

                        m_CachedDurations.Add(stateName, stateMotion.length);
                    }
                }
                catch (System.Exception)
                {
                    var warning = new string[]
                    {
                        $"[WARNING] Could not find {nameof(Agents.BehaviourAgent)} in the scene!"
                    };

                    m_CachedIds = warning.Select(c => c);
                }
            }

            return m_CachedIds;
        }

        protected override void OnItemSelected(object item)
        {
            base.OnItemSelected(item);

            var itemIdProp = (IdArgument)item;


            if (string.IsNullOrEmpty(itemIdProp.Id))
                return;

            var durationProp = itemIdProp.Property.serializedObject.FindProperty("_animationDuration");

            durationProp.floatValue = m_CachedDurations[itemIdProp.Id];

            itemIdProp.Property.serializedObject.ApplyModifiedProperties();
        }
    }
}