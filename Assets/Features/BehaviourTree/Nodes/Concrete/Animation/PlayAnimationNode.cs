using System;
using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [Serializable]
    public class AnimationName : ICloneable
    {
        public string Name;

        public object Clone()
        {
            return new AnimationName { Name = (string)this.Name.Clone() };
        }

        public static implicit operator string(AnimationName target) => target.Name;
    }

    [NodeInfo("execute", "Play Animation", "Animation/Play Animation")]
    public class PlayAnimationNode : ActionNode
    {
        [SerializeField, DisplayOnNode] private AnimationName _animationName;
        [SerializeField, DisplayOnNode] private bool _waitForAnimation;

        [SerializeField, ReadOnly, DisplayOnNode] private float _animationDuration;
        [SerializeField, ReadOnly, DisplayOnNode] private float _timer;



        protected override void OnStartInternal()
        {
            base.OnStartInternal();

            m_Tree.Agent.Animator.Play(_animationName, 0, 0f);

            if (_waitForAnimation)
                _timer = _animationDuration;
        }

        protected override State OnUpdateInternal()
        {
            _timer -= Time.deltaTime;

            return _timer < 0f ? State.Success : State.Running;
        }
    }
}