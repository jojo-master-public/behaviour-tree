using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    public enum PositionDirection
    {
        ABOVE,
        BELOW,
        LEFT,
        RIGHT,
        HORIZONTAL,
        VERTICAL,
        DIAGONAL_ABOVE,
        DIAGONAL_BELOW,
        CUSTOM_DIRECTION,
    }

    [NodeInfo("execute", "GetSuitablePosition", "FindingPosition/Get Suitable Position")]
    public class GetSuitablePositionNode : ActionNode
    {
        [SerializeField] private PositionDirection m_Direction;
        [SerializeField] private Vector3 m_CustomDirection;
        [SerializeField] private float m_Distance;
        [SerializeField] private float m_Error;



        protected override State OnUpdateInternal()
        {
            if (!Tree.Agent.ReadData("target", out Transform target))
                return State.Failure;

            Vector3 suitablePosition;
            var selfPosition = Tree.Agent.Position;

            var randomError = Random.Range(-1f, 1f) * m_Error;
            var dist = m_Distance + randomError;
            Vector3 direction = Vector3.zero;

            switch (m_Direction)
            {
                case PositionDirection.ABOVE:
                    direction = Vector3.up;
                    break;
                case PositionDirection.BELOW:
                    direction = Vector3.down;
                    break;
                case PositionDirection.LEFT:
                    direction = Vector3.left;
                    break;
                case PositionDirection.RIGHT:
                    direction = Vector3.right;
                    break;
                case PositionDirection.DIAGONAL_ABOVE:
                    var towardsTarget1 = Mathf.Sign(selfPosition.x - target.position.x);

                    direction = new Vector3(towardsTarget1, 1).normalized;
                    break;
                case PositionDirection.HORIZONTAL:
                    var horizontal = Mathf.Sign(selfPosition.x - target.position.x);

                    direction = new Vector3(horizontal, 0).normalized;
                    break;
                case PositionDirection.VERTICAL:
                    var vertical = Mathf.Sign(selfPosition.y - target.position.y);

                    direction = new Vector3(0, vertical).normalized;
                    break;
                case PositionDirection.DIAGONAL_BELOW:
                    var towardsTarget2 = Mathf.Sign(selfPosition.x - target.position.x);

                    direction = new Vector3(towardsTarget2, -1).normalized;
                    break;
                case PositionDirection.CUSTOM_DIRECTION:
                    direction = m_CustomDirection.normalized;
                    break;
                default:
                    break;
            }

            suitablePosition = target.position + direction * dist;
            Tree.Agent.WriteData(suitablePosition, "suitablePosition");

            return State.Success;
        }
    }
}