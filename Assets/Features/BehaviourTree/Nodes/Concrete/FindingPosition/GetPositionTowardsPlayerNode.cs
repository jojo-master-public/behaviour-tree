using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("execute", "Get Position Near Player", "FindingPosition/Get Position Near Player")]
    public class GetPositionTowardsPlayerNode : ActionNode
    {
        private const string TargetKey = "target";
        private const string UseDistToPlayerKey = "useDistToPlayer";
        private const string DistanceToPlayerKey = "distanceToPlayer";
        private const string SuitablePositionKey = "suitablePosition";

        [SerializeField] private float m_Distance;
        [SerializeField] private float m_Error;



        protected override State OnUpdateInternal()
        {
            if (!Tree.Agent.ReadData(TargetKey, out Transform target))
                return State.Failure;

            Tree.Agent.ReadData(UseDistToPlayerKey, out bool useDistToPlayer);

            var distance = m_Distance;

            if (useDistToPlayer)
            {
                if (Tree.Agent.ReadData(DistanceToPlayerKey, out float distToPlayer))
                    distance = distToPlayer;
                else
                    return State.Failure;
            }

            Vector3 suitablePosition;
            var selfPosition = Tree.Agent.Position;

            var randomError = Random.Range(-1f, 1f) * m_Error;
            var dist = distance + randomError;
            Vector3 direction = (selfPosition - target.position).normalized;

            suitablePosition = target.position + direction * dist;
            Tree.Agent.WriteData(suitablePosition, SuitablePositionKey);

            return State.Success;
        }
    }
}