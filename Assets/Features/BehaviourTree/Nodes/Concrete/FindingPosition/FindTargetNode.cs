using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("execute", "Find Target", "FindingPosition/Find Target", true, false)]
    public class FindTargetNode : ActionNode
    {
        [SerializeField] private string m_Tag;



        protected override State OnUpdateInternal()
        {
            try
            {
                if (string.IsNullOrEmpty(m_Tag))
                {
                    throw new System.ArgumentException($"[{Tree.name}:{name}:OnUpdateInternal] m_Tag is empty!");
                }
            }
            catch (System.ArgumentException ex)
            {
                return State.Failure;
            }

            var target = GameObject.FindWithTag(m_Tag);

            if (target == null)
                return State.Failure;

            Tree.Agent.WriteData(target.transform, "target");

            return State.Success;
        }
    }
}