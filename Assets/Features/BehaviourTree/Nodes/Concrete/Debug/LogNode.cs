using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    [NodeInfo("execute", "Log", "Debug/Log", true, true)]
    public class LogNode : DecoratorNode
    {
        [SerializeField, TextArea(3, 6)] private string m_Message;



        protected override void OnStartInternal()
        {
            UnityEngine.Debug.Log($"[LogNode] {m_Message}");
        }
    }
}