using System;

namespace Project.AI.BehaviourTree.Nodes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
    public class NodeInfoAttribute : Attribute
    {
        public readonly string Tag;
        public readonly string NodeDisplayName;
        public readonly string MenuName;
        public readonly bool InputExecution;
        public readonly bool OutputExecution;


        public NodeInfoAttribute(string tag, string nodeDisplayName, string menuName, bool inputExecution = true, bool outputExecution = true)
        {
            this.Tag = tag;
            this.NodeDisplayName = nodeDisplayName;
            this.InputExecution = inputExecution;
            this.OutputExecution = outputExecution;
            this.MenuName = menuName;
        }
    }
}