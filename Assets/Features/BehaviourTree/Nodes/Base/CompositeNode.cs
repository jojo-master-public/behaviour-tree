namespace Project.AI.BehaviourTree.Nodes
{
    public abstract class CompositeNode : Node
    {
        public void AddNodePort()
        {
            ExecutionPorts.Add(new NodePort());
        }

        public void RemoveNodePort()
        {
            if(ExecutionPorts.Count > 1)
                ExecutionPorts.RemoveAt(ExecutionPorts.Count - 1);
        }

        public void RemoveNodePort(int index)
        {
            if(ExecutionPorts.Count > index)
                ExecutionPorts.RemoveAt(index);
        }

        public override void Reset()
        {
            base.Reset();

            foreach (var port in ExecutionPorts)
            {
                var child = Tree.GetNodeByGuid(port.output.guid);

                child.Reset();
            }
        }
    }
}