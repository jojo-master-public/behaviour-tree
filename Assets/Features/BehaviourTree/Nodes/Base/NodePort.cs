using System;
using System.Collections.Generic;

namespace Project.AI.BehaviourTree.Nodes
{
    public struct PortInfo
    {
        public readonly Type type;
        public readonly string fieldName;
        public readonly string displayName;

        public PortInfo(string fieldName, string displayName, Type type)
        {
            this.fieldName = fieldName;
            this.displayName = displayName;
            this.type = type;
        }
    }

    [Serializable]
    public class OutputInfo
    {
        public string guid = string.Empty;
        public int index = -1;

        public OutputInfo(string guid, int index)
        {
            this.guid = guid;
            this.index = index;
        }

        public OutputInfo(OutputInfo source)
        {
            this.guid = source.guid;
            this.index = source.index;
        }
    }

    [Serializable]
    public class NodePort
    {
        public OutputInfo output;

        public NodePort()
        {
            output = new OutputInfo(string.Empty, -1);
        }

        public NodePort(string inputGuid, int inputIndex)
        {
            output = new OutputInfo(inputGuid, inputIndex);
        }

        public NodePort(NodePort port)
        {
            output = new OutputInfo(port.output.guid, port.output.index);
        }
    }
}