namespace Project.AI.BehaviourTree.Nodes
{
    public abstract class DecoratorNode : Node
    {
        protected override State OnUpdateInternal()
        {
            return UpdateChild();
        }

        protected State UpdateChild()
        {
            var guid = ExecutionPorts[0].output.guid;

            if (string.IsNullOrEmpty(guid))
            {
                return State.Success;
            }
            else
            {
                var childNode = Tree.GetNodeByGuid(guid);

                if (childNode != null)
                    return childNode.Update();
                else
                    return State.Success;
            }
        }

        public override void Reset()
        {
            base.Reset();

            ResetChild();
        }

        public void ResetChild()
        {
            var child = Tree.GetNodeByGuid(ExecutionPorts[0].output.guid);

            child?.Reset();
        }
    }
}