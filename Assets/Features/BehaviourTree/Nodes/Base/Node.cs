using System.Collections.Generic;
using UnityEngine;

namespace Project.AI.BehaviourTree.Nodes
{
    public abstract class Node : ScriptableObject
    {
        public enum State
        {
            Idle,
            Running,
            Success,
            Failure
        }

        public State CurrentState { get; private set; } = State.Idle;

        [SerializeField, HideInInspector] private List<NodePort> m_ExecutionPorts = new List<NodePort>();
        [SerializeField, HideInInspector] private string m_Guid;

        protected BehaviourTree m_Tree;

        public List<NodePort> ExecutionPorts { get => m_ExecutionPorts; set => m_ExecutionPorts = value; }
        public BehaviourTree Tree => m_Tree;
        public string Guid { get => m_Guid; set => m_Guid = value; }



        #region PUBLIC

        public void InitNode(BehaviourTree tree)
        {
            this.m_Tree= tree;

            InitOutputs();
        }

        public virtual void ConnectExecutePort(Node node, int outputIndex, int inputIndex = 0)
        {
            if (outputIndex >= ExecutionPorts.Count)
                return;

            NodePort output = ExecutionPorts[outputIndex];

            OutputInfo connection = new OutputInfo(node.Guid, inputIndex);

            output.output = connection;

            ExecutionPorts[outputIndex] = output;
        }

        public virtual void DisconnectExecutePort(Node node, int outputIndex)
        {
            ExecutionPorts[outputIndex].output.guid = null;
            ExecutionPorts[outputIndex].output.index = -1;
        }

        public bool HasExecuteConnections()
        {
            return ExecutionPorts != null && ExecutionPorts.Count > 0;
        }

        public State Update()
        {
            if(CurrentState == State.Idle)
            {
                OnStartInternal();
            }

            CurrentState = OnUpdateInternal();

            if(CurrentState == State.Failure || CurrentState == State.Success)
            {
                OnStopInternal();
            }

            return CurrentState;
        }

        public virtual void Reset()
        {
            CurrentState = State.Idle;
        }

        #endregion

        #region PROTECTED

        protected virtual void OnStartInternal() { }
        protected virtual void OnStopInternal() { }
        protected abstract State OnUpdateInternal();

        protected virtual void InitOutputs()
        {
            if (ExecutionPorts.Count == 0)
            {
                ExecutionPorts.Add(new NodePort());
            }
        }
        protected virtual void InitData() { }

        #endregion

        #region EDITOR

        [SerializeField, HideInInspector] private Vector2 m_Position;

        public Vector2 Position { get => m_Position; set => m_Position = value; }

        #endregion
    }
}