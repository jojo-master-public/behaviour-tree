namespace Project.AI.BehaviourTree.Nodes
{
    public abstract class ActionNode : Node
    {
        protected override void InitOutputs()
        {
            ExecutionPorts = null;
        }
    }
}