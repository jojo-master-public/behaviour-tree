using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Project.AI.BehaviourTree.Agents;
using Project.AI.BehaviourTree.Nodes;

namespace Project.AI.BehaviourTree
{
    [CreateAssetMenu(fileName = "Tree_", menuName = "Project/AI/New Tree")]
    public class BehaviourTree : ScriptableObject
    {
        [SerializeField] private Node m_Root;
        [SerializeField] private List<Node> m_Nodes = new List<Node>();

        public Node.State TreeState { get; private set; }
        public BehaviourAgent Agent { get; private set; }
        public List<Node> Nodes => m_Nodes;
        public Node Root => m_Root;



        public BehaviourTree Clone()
        {
            var treeClone = Instantiate(this);

            treeClone.m_Root = Instantiate(m_Root);
            treeClone.m_Root.InitNode(treeClone);

            List<Node> nodes = new List<Node>();

            foreach (var n in treeClone.Nodes)
            {
                var nodeClone = Instantiate(n);

                nodeClone.InitNode(treeClone);
                nodes.Add(nodeClone);
            }

            treeClone.m_Nodes = nodes;

            return treeClone;
        }

        public void Play(BehaviourAgent agent)
        {
            Agent = agent;
            TreeState = Node.State.Running;
        }

        public Node.State Update()
        {
            if(TreeState == Node.State.Running)
                TreeState = m_Root.Update();

            return TreeState;
        }

        public Node GetNodeByGuid(string guid)
        {
            return m_Nodes.FirstOrDefault(n => n.Guid == guid);
        }

        public void SetRootNode(Node node)
        {
            this.m_Root = node;
        }
    }
}