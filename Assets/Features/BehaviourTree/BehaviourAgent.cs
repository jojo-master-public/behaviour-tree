using Project.Enemies.Behaviours;
using System.Collections.Generic;
using UnityEngine;

namespace Project.AI.BehaviourTree.Agents
{
    [System.Serializable]
    public struct FloatData
    {
        public string Key;
        public float Value;
    }

    [System.Serializable]
    public struct BoolData
    {
        public string Key;
        public bool Value;
    }

    public class BehaviourAgent : MonoBehaviour
    {
        [SerializeField] private BehaviourTree m_InitialTree;
        [SerializeField] private FloatData[] m_FloatData;
        [SerializeField] private BoolData[] m_BoolData;

        private Dictionary<string, object> m_Data = new Dictionary<string, object>();
        public Dictionary<string, object> Data => m_Data;

        public Vector3 Position => transform.position;
        public MovementBehaviourBase Movement { get; private set; }
        public AttackBehaviourBase Attack { get; private set; }
        public BehaviourTreeRunner TreeRunner { get; private set; }
        public Animator Animator { get; private set; }
        public Enemy Enemy { get; private set; }



        public bool WriteData(object data, string key, bool overwrite = true)
        {
            if(m_Data.ContainsKey(key))
            {
                if(overwrite)
                {
                    m_Data[key] = data;
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                m_Data.Add(key, data);
                return true;
            }
        }

        public bool ReadData<T>(string key, out T data)
        {
            if (m_Data.ContainsKey(key))
            {
                data = (T)m_Data[key];
                return true;
            }
            else
            {
                data = default;
                return false;
            }
        }

        private void Awake()
        {
            Movement = GetComponent<MovementBehaviourBase>();
            Attack = GetComponent<AttackBehaviourBase>();

            TreeRunner = GetComponent<BehaviourTreeRunner>();
            Animator = GetComponentInChildren<Animator>();
            Enemy = GetComponent<Enemy>();

            Enemy.onReturnToPool += OnEnemyReturnedToPool;

        }

        private void OnEnemyReturnedToPool(IPoolableObject obj)
        {
            m_Data.Clear();
        }

        private void FillData()
        {
            foreach (var fData in m_FloatData)
            {
                m_Data.Add(fData.Key, fData.Value);
            }

            foreach (var bData in m_BoolData)
            {
                m_Data.Add(bData.Key, bData.Value);
            }
        }

        private void OnEnable()
        {
            FillData();
            TreeRunner.Play(m_InitialTree, this);
        }
    }
}