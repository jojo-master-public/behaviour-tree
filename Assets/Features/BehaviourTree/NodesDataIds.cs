namespace Project.AI.BehaviourTree
{
    public class NodesDataIds
    {
        public const string PLAYER_TRANSFORM = "PlayerTransform";
        public const string TARGET_POSITION = "TargetPosition";
        public const string TARGET_TRANSFORM = "Target";
    }
}