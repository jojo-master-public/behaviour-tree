using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Project.AI.BehaviourTree.Nodes;

namespace Project.AI.BehaviourTree
{
    public class BehaviourTreeEditorWindow : EditorWindow
    {
        [SerializeField] private BehaviourTree m_TreeAsset;

        private BehaviourTreeView m_BehaviourTreeView;
        private InspectorView m_InspectorView;
        private ToolbarMenu m_ToolbarMenu;
        private BehaviourTreeEditorWindow m_WndCached;



        [MenuItem("BehaviourTree/Editor Window")]
        public static BehaviourTreeEditorWindow OpenWindow()
        {
            BehaviourTreeEditorWindow wnd = GetWindow<BehaviourTreeEditorWindow>();
            wnd.titleContent = new GUIContent("Behaviour Tree Editor");
            wnd.position = new Rect(160, 120, 1000, 750);

            return wnd;
        }

        [OnOpenAsset]
        public static bool OnOpenAsset(int instanceId, int line)
        {
            if (Selection.activeObject is BehaviourTree tree)
            {
                BehaviourTreeEditorWindow wnd = OpenWindow();

                if (tree.Root == null)
                    tree.SetRootNode(CreateNode(typeof(RootNode), tree, Vector2.zero));

                wnd.InitEditor(tree);

                return true;
            }

            return false;
        }

        public static Node CreateNode(System.Type type, BehaviourTree tree, Vector2 position)
        {
            Node newNode = ScriptableObject.CreateInstance(type) as Node;
            newNode.name = type.Name;
            newNode.Guid = GUID.Generate().ToString();
            newNode.InitNode(tree);
            newNode.Position = position;

            Undo.RecordObject(tree, "BehaviourTree (Add Node)");

            tree.Nodes.Add(newNode);

            if (!Application.isPlaying)
                AssetDatabase.AddObjectToAsset(newNode, tree);

            Undo.RegisterCreatedObjectUndo(newNode, "BehaviourTree (Add Node)");
            AssetDatabase.SaveAssets();

            return newNode;
        }

        public static void DeleteNode(Node target, BehaviourTree tree)
        {
            Undo.RecordObject(tree, "SkillTree (Delete Node)");

            tree.Nodes.Remove(target);

            Undo.DestroyObjectImmediate(target);

            AssetDatabase.SaveAssets();
        }

        public void InitEditor(BehaviourTree treeAsset = null)
        {
            if (treeAsset != null)
                m_TreeAsset = treeAsset;

            m_BehaviourTreeView.PopulateView(m_TreeAsset);
        }

        public void CreateGUI()
        {
            VisualElement root = rootVisualElement;

            var visualTree = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Features/BehaviourTree/Editor/BehaviourTreeEditor.uxml");
            visualTree.CloneTree(root);

            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Features/BehaviourTree/Editor/BehaviourTreeEditor.uss");
            root.styleSheets.Add(styleSheet);

            m_BehaviourTreeView = root.Q<BehaviourTreeView>();
            m_BehaviourTreeView.Init(this);
            m_BehaviourTreeView.onNodeSelected += OnNodeSelectionChange;
            m_InspectorView = root.Q<InspectorView>();
            m_ToolbarMenu = root.Q<ToolbarMenu>();

            if(m_TreeAsset != null)
            {
                m_BehaviourTreeView.PopulateView(m_TreeAsset);
            }

            FillToolbarMenus(m_ToolbarMenu);
        }

        private void FillToolbarMenus(ToolbarMenu menu)
        {
            menu.menu.AppendAction("ReInit Nodes", ReinitNodes);
            menu.menu.AppendAction("Get Active Tree", GetActiveTree);
        }

        public static void ConnectExecuteNodes(Node from, Node to, int fromIndex, int toIndex = 0)
        {
            Undo.RecordObject(from, "SkillTree (Add Connection)");

            from.ConnectExecutePort(to, fromIndex, toIndex);

            EditorUtility.SetDirty(from);
        }

        public static void DisconnectExecuteNode(Node from, Node to, int fromIndex)
        {
            if (from == null)
                return;

            Undo.RecordObject(from, "SkillTree (Remove Connection)");

            from.DisconnectExecutePort(to, fromIndex);

            EditorUtility.SetDirty(from);
        }

        public void OnTreeStarted(BehaviourTreeRunner treeRunner)
        {
            InitEditor(treeRunner.CurrentTree);
        }

        public void OnEditorUpdate()
        {
            m_BehaviourTreeView.UpdateViewStates();
        }

        private void OnNodeSelectionChange(NodeView view)
        {
            m_InspectorView.UpdateSelection(view);
        }

        private void ReinitNodes(DropdownMenuAction a)
        {
            if(m_TreeAsset != null)
            {
                foreach (var n in m_TreeAsset.Nodes)
                {
                    n.InitNode(m_TreeAsset);
                }

                m_TreeAsset.Root.InitNode(m_TreeAsset);
            }
        }

        private void GetActiveTree(DropdownMenuAction a)
        {
            var selectedGO = Selection.activeGameObject;

            if(selectedGO != null)
            {
                var runner = selectedGO.GetComponent<BehaviourTreeRunner>();

                if(runner != null)
                    InitEditor(runner.CurrentTree);
            }
            else if (Selection.activeObject is BehaviourTree tree)
            {
                InitEditor(tree);
            }
        }
    }
}