using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEditor.UIElements;
using UnityEngine;
using UnityEngine.UIElements;
using Project.AI.BehaviourTree.Nodes;
using Node = Project.AI.BehaviourTree.Nodes.Node;

namespace Project.AI.BehaviourTree
{
    public class NodeView : UnityEditor.Experimental.GraphView.Node
    {
        public Action<NodeView> onNodeSelected;

        public Node node;

        public Port inputExecutionPort;
        public List<Port> outputExecutionPorts;

        public List<Port> inputs;
        public List<Port> outputs;

        private NodeInfoAttribute metadata;
        private VisualElement m_VariablesContainer;



        public NodeView(Node node) : base("Assets/Features/BehaviourTree/Editor/NodeView.uxml")
        {
            this.node = node;
            this.viewDataKey = node.Guid;

            style.left = node.Position.x;
            style.top = node.Position.y;

            metadata = (NodeInfoAttribute)node.GetType().GetCustomAttributes(typeof(NodeInfoAttribute), true).FirstOrDefault(x => x is NodeInfoAttribute);
            m_VariablesContainer = this.Q<VisualElement>("variables-container");

            CreateInputPorts();
            CreateOutputPorts();
            SetupClasses();
            CreateDisplayProperties();

            this.title = metadata.NodeDisplayName;
        }

        public void UpdateState()
        {
            RemoveFromClassList("idle");
            RemoveFromClassList("running");
            RemoveFromClassList("success");
            RemoveFromClassList("error");

            if (Application.isPlaying)
            {
                switch (node.CurrentState)
                {
                    case Node.State.Idle:
                        AddToClassList("idle");
                        break;
                    case Node.State.Running:
                        AddToClassList("running");
                        break;
                    case Node.State.Success:
                        AddToClassList("success");
                        break;
                    case Node.State.Failure:
                        AddToClassList("error");
                        break;
                }
            }
        }

        public override void SetPosition(Rect newPos)
        {
            base.SetPosition(newPos);

            Undo.RecordObject(node, "SkillTree (Set Position)");

            node.Position = new Vector2(newPos.xMin, newPos.yMin);

            EditorUtility.SetDirty(node);
        }

        public override void OnSelected()
        {
            base.OnSelected();

            onNodeSelected?.Invoke(this);
        }

        public void SetPortsConnectionState(List<Port> ports, int portIndex, bool isHidden)
        {
            if (ports.Count <= portIndex)
                return;

            var children = ports[portIndex].Children();

            foreach (var child in children)
            {
                if (child is PropertyField)
                {
                    if (isHidden)
                        child.AddToClassList("hidden-property-field");
                    else
                        child.RemoveFromClassList("hidden-property-field");
                }
            }
        }

        private void SetupClasses()
        {
            switch (metadata.Tag)
            {
                case "decorator":
                    AddToClassList("decorator-node");
                    break;
                case "execute":
                    AddToClassList("execute-node");
                    break;
                case "composite":
                    AddToClassList("composite-node");
                    break;
                case "sub-graph":
                    AddToClassList("sub-graph-node");
                    break;
                case "root":
                    AddToClassList("root-node");
                    break;
            }
        }

        private void CreateDisplayProperties()
        {
            var type = this.node.GetType();
            var displayFields = type.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic)
                                    .Where(f => f.GetCustomAttribute<DisplayOnNodeAttribute>() != null);

            if (displayFields != null && displayFields.Count() > 0)
            {
                var serializedObject = new SerializedObject(this.node);

                foreach (var f in displayFields)
                {
                    var serializedProp = serializedObject.FindProperty(f.Name);
                    var propertyField = new PropertyField(serializedProp, serializedProp.displayName);
                    propertyField.BindProperty(serializedObject);

                    m_VariablesContainer.Add(propertyField);
                }
            }
            else
            {
                m_VariablesContainer.RemoveFromClassList("hidden-property-field");
                m_VariablesContainer.AddToClassList("hidden-property-field");
            }
        }

        private void CreateInputPorts()
        {
            var nodeInfo = (NodeInfoAttribute)node.GetType().GetCustomAttribute(typeof(NodeInfoAttribute), true);

            if (nodeInfo.InputExecution)
            {
                inputExecutionPort = InstantiatePort(Orientation.Horizontal, Direction.Input, Port.Capacity.Single, null);
                inputExecutionPort.portName = "input";
                inputExecutionPort.portColor = Color.red;

                inputContainer.Add(inputExecutionPort);
            }
            else
            {
                inputContainer.parent.RemoveFromClassList("align-items-right");
                inputContainer.parent.AddToClassList("align-items-right");
            }
        }

        private void CreateOutputPorts()
        {
            var nodeInfo = (NodeInfoAttribute)node.GetType().GetCustomAttribute(typeof(NodeInfoAttribute), true);

            outputExecutionPorts = new List<Port>();

            if (nodeInfo.OutputExecution && node.ExecutionPorts != null)
            {
                for (int i = 0; i < node.ExecutionPorts.Count; i++)
                {
                    Port p = InstantiatePort(Orientation.Horizontal, Direction.Output, Port.Capacity.Single, null);

                    p.portName = node.ExecutionPorts.Count > 1 ? $"out_{i}" : "out";
                    p.portColor = Color.red;

                    outputExecutionPorts.Add(p);
                    outputContainer.Add(p);
                }
            }
        }
    }
}