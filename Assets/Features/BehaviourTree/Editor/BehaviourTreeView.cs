using UnityEngine.UIElements;
using UnityEditor.Experimental.GraphView;
using UnityEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Project.AI.BehaviourTree.Utility;
using Project.AI.BehaviourTree.Nodes;
using Node = Project.AI.BehaviourTree.Nodes.Node;

namespace Project.AI.BehaviourTree
{
    public class BehaviourTreeView : GraphView
    {
        public new class UxmlFactory : UxmlFactory<BehaviourTreeView, GraphView.UxmlTraits> { }

        public event Action<NodeView> onNodeSelected;

        private BehaviourTree m_CurrentTree; 
        private EditorWindow m_CurrentEditor;
        private List<Node> m_NodesToCopy = new List<Node>();
        private BehaviourTreeSearchProvider m_SearchProvider;



        public BehaviourTreeView()
        {
            Insert(0, new GridBackground());

            var contentZoomer = new ContentZoomer();

            contentZoomer.minScale = 0.15f;
            contentZoomer.referenceScale = 2f;
            contentZoomer.maxScale = 3f;

            this.AddManipulator(contentZoomer);
            this.AddManipulator(new ContentDragger());
            this.AddManipulator(new SelectionDragger());
            this.AddManipulator(new RectangleSelector());

            var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Features/BehaviourTree/Editor/BehaviourTreeEditor.uss");
            styleSheets.Add(styleSheet);

            Undo.undoRedoPerformed += OnUndoRedo;
            RegisterCallback<KeyDownEvent>(HandleKeyboard);

            this.serializeGraphElements += CopyOperation;
            this.unserializeAndPaste += PasteOperation;
        }

        public void PopulateView(BehaviourTree tree)
        {
            m_CurrentTree = tree;

            var elements = graphElements.ToList();

            graphViewChanged -= OnGraphViewChanged;

            DeleteElements(elements);

            graphViewChanged += OnGraphViewChanged;

            m_SearchProvider = ScriptableObject.CreateInstance<BehaviourTreeSearchProvider>();
            m_SearchProvider.Init(this, m_CurrentEditor);

            tree.Nodes.ForEach(CreateNodeView);

            tree.Nodes.ForEach(n =>
            {
                NodeView parentView = FindNodeView(n.Guid);

                if (n.ExecutionPorts != null)
                {
                    for (int i = 0; i < n.ExecutionPorts.Count; i++)
                    {
                        OutputInfo o = n.ExecutionPorts[i].output;

                        NodeView childView = FindNodeView(o.guid);

                        if (childView != null)
                        {
                            Edge edge = parentView.outputExecutionPorts[i].ConnectTo(childView.inputExecutionPort);

                            AddElement(edge);
                        }
                    }
                }
            });
        }

        public void Init(BehaviourTreeEditorWindow editor)
        {
            this.m_CurrentEditor = editor;
        }

        public override void BuildContextualMenu(ContextualMenuPopulateEvent evt)
        {
            var allTypes = TypeCache.GetTypesDerivedFrom<Node>();
            var concreteType = allTypes.Where(t => !t.IsAbstract);

            foreach (var t in concreteType)
            {
                NodeInfoAttribute metadata = (NodeInfoAttribute)t.GetCustomAttributes(typeof(NodeInfoAttribute), true).FirstOrDefault(x => x is NodeInfoAttribute);

                if (metadata != null)
                    evt.menu.AppendAction($"{metadata.MenuName}", a => CreateNode(t, a.eventInfo.mousePosition));
                else
                    UnityEngine.Debug.LogError($"Node {t.Name} is missing NodeInfoAttribute");
            }

            //var subTreeAssets = AssetDatabase.FindAssets("SubTree_");

            //foreach (var subtreepath in subTreeAssets)
            //{
            //    var subtree = AssetDatabase.LoadAssetAtPath<SubSkillTree>(AssetDatabase.GUIDToAssetPath(subtreepath));

            //    evt.menu.AppendAction($"SubTree/Nodes/{subtree.name.Substring(8)}", a =>
            //    {
            //        var node = CreateNode<SubGraphNode>(a.eventInfo.mousePosition);
            //        node.targetTree = subtree;

            //        PopulateView(Tree);
            //    });
            //}
        }

        public override List<Port> GetCompatiblePorts(Port startPort, NodeAdapter nodeAdapter)
        {
            CustomNodeAdapter nda = new CustomNodeAdapter();

            var validPorts = ports
                .ToList()
                .Where(endPort =>
                    endPort.direction != startPort.direction &&
                    endPort.node != startPort.node &&
                    ((nda.CanAdapt(startPort.source, endPort.source) && nda.Connect(startPort.source, endPort.source)) ||
                    (endPort.portType == startPort.portType)))
                    .ToList();

            return validPorts;
        }

        public NodeView FindNodeView(string nodeGuid)
        {
            return GetNodeByGuid(nodeGuid) as NodeView;
        }

        public void UpdateViewStates()
        {
            foreach (var n in nodes)
            {
                ((NodeView)n).UpdateState();
            }
        }

        private void CreateNodeView(Node node)
        {
            NodeView nodeView = new NodeView(node);
            nodeView.onNodeSelected = onNodeSelected;

            AddElement(nodeView);
        }

        private GraphViewChange OnGraphViewChanged(GraphViewChange args)
        {
            try
            {
                args.elementsToRemove?.ForEach(OnGraphElementRemoved);
                args.edgesToCreate?.ForEach(OnEdgeCreated);
            }
            catch (InvalidOperationException ex)
            {
                PopulateView(m_CurrentTree);
            }

            return args;
        }

        private void OnUndoRedo()
        {
            PopulateView(m_CurrentTree);

            AssetDatabase.SaveAssets();
        }

        private void HandleKeyboard(KeyDownEvent callback)
        {
            switch (callback.keyCode)
            {
                case KeyCode.D:
                    if (!callback.ctrlKey)
                        return;

                    DuplicateSelection(selection);
                    break;
                case KeyCode.Space:
                    //SearchWindow.Open(new SearchWindowContext(Event.current.mousePosition), provider);
                    break;
            }
        }

        public void CreateNode(Type a, Vector2 position)
        {
            var worldMousePosition = m_CurrentEditor.rootVisualElement.ChangeCoordinatesTo(m_CurrentEditor.rootVisualElement.parent, position - m_CurrentEditor.position.position);
            var localMousePosition = contentViewContainer.WorldToLocal(worldMousePosition) + new Vector2(150, 80);

            Node node = BehaviourTreeEditorWindow.CreateNode(a, m_CurrentTree, localMousePosition);

            CreateNodeView(node);
        }

        private T CreateNode<T>(Vector2 position) where T : Node
        {
            var worldMousePosition = m_CurrentEditor.rootVisualElement.ChangeCoordinatesTo(m_CurrentEditor.rootVisualElement.parent, position - m_CurrentEditor.position.position);
            var localMousePosition = contentViewContainer.WorldToLocal(worldMousePosition) + new Vector2(150, 80);

            T node = (T)BehaviourTreeEditorWindow.CreateNode(typeof(T), m_CurrentTree, localMousePosition);

            CreateNodeView(node);

            return node;
        }

        private void OnEdgeCreated(Edge edge)
        {
            NodeView parentView = edge.output.node as NodeView;
            NodeView childView = edge.input.node as NodeView;

            if (parentView.outputExecutionPorts.Contains(edge.output))
            {
                int outputPortIndex = parentView.outputExecutionPorts.IndexOf(edge.output);

                BehaviourTreeEditorWindow.ConnectExecuteNodes(parentView.node, childView.node, outputPortIndex);
            }
        }

        private void OnGraphElementRemoved(GraphElement elem)
        {
            NodeView nodeView = elem as NodeView;

            if (nodeView != null)
            {
                BehaviourTreeEditorWindow.DeleteNode(nodeView.node, m_CurrentTree);
            }

            Edge edge = elem as Edge;

            if (edge != null && edge.output != null && edge.input != null)
            {
                NodeView parentView = edge.output.node as NodeView;
                NodeView childView = edge.input.node as NodeView;

                if (parentView.outputExecutionPorts.Contains(edge.output))
                {
                    int index = parentView.outputExecutionPorts.IndexOf(edge.output);

                    BehaviourTreeEditorWindow.DisconnectExecuteNode(parentView.node, childView.node, index);
                }
            }
        }

        #region NODES COPY/PASTE

        private void DuplicateSelection(List<ISelectable> selection)
        {
            List<Node> nodes = new List<Node>();

            selection.ForEach(x =>
            {
                if (x is NodeView)
                    nodes.Add((x as NodeView).node);
            });

            PasteNodes(nodes);
        }

        private string CopyOperation(IEnumerable<GraphElement> elements)
        {
            m_NodesToCopy.Clear();

            foreach (GraphElement n in elements)
            {
                NodeView nodeView = n as NodeView;

                if (nodeView != null)
                    m_NodesToCopy.Add(nodeView.node);
            }

            return "Copy Nodes";
        }

        private void PasteOperation(string operationName, string data)
        {
            if (operationName != "Paste")
                return;

            PasteNodes(m_NodesToCopy);
        }

        private void PasteNodes(List<Node> nodesToDuplicate)
        {
            List<string> clonedGuids = new List<string>();

            Dictionary<string, string> clonedGuidToOldGuid = new Dictionary<string, string>();
            Dictionary<string, string> oldGuidToClonedGuid = new Dictionary<string, string>();

            List<Node> duplicatedNodes = new List<Node>();

            foreach (Node originalNode in nodesToDuplicate)
            {
                Node clone = BehaviourTreeEditorWindow.CreateNode(originalNode.GetType(), m_CurrentTree, originalNode.Position);

                clone.Guid = GUID.Generate().ToString();
                clone.name = originalNode.name;
                clone.Position = clone.Position + new Vector2(20, -20);
                clone.InitNode(m_CurrentTree);

                clonedGuidToOldGuid.Add(clone.Guid, originalNode.Guid);
                oldGuidToClonedGuid.Add(originalNode.Guid, clone.Guid);

                clonedGuids.Add(clone.Guid);

                duplicatedNodes.Add(clone);
            }

            foreach (var node in duplicatedNodes)
            {
                Node originalNode = m_CurrentTree.GetNodeByGuid(clonedGuidToOldGuid[node.Guid]);

                if (originalNode == null)
                    originalNode = nodesToDuplicate.First(x => x.Guid == clonedGuidToOldGuid[node.Guid]);

                if (originalNode.ExecutionPorts != null && node.ExecutionPorts != null)
                {
                    int portCountDiff = originalNode.ExecutionPorts.Count - node.ExecutionPorts.Count;

                    for (int i = 0; i < portCountDiff; i++)
                    {
                        node.ExecutionPorts.Add(new NodePort());
                    }

                    for (int i = 0; i < originalNode.ExecutionPorts.Count; i++)
                    {
                        if (originalNode.ExecutionPorts[i].output.index < 0)
                            continue;

                        if (oldGuidToClonedGuid.ContainsKey(originalNode.ExecutionPorts[i].output.guid))
                        {
                            node.ExecutionPorts[i]
                                .output = new OutputInfo(oldGuidToClonedGuid[originalNode.ExecutionPorts[i].output.guid],
                                originalNode.ExecutionPorts[i].output.index);
                        }
                    }
                }
            }

            PopulateView(m_CurrentTree);

            foreach (var guid in clonedGuids)
            {
                NodeView nv = GetNodeByGuid(guid) as NodeView;

                AddToSelection(nv);
            }

            m_NodesToCopy.Clear();
        }

        #endregion
    }
}