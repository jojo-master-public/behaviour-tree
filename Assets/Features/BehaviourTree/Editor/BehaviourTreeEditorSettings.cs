using UnityEngine;

namespace Project.AI.BehaviourTree.Editors
{
    [CreateAssetMenu(fileName = "Settings_BehaviourTreeEditor", menuName = "Project/AI/New Settings")]
    public class BehaviourTreeEditorSettings : ScriptableObject
    {
        public const string PATH = "Assets/Features/BehaviourTree/Editor/Settings_BehaviourTreeEditor.asset";

        [SerializeField] private Color m_PortColor = new Color(0.7f, 0.7f, 0.7f, 1.0f);

        public Color PortColor => m_PortColor;
    }
}