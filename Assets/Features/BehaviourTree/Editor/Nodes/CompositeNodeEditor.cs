using UnityEngine;
using UnityEditor;
using Project.AI.BehaviourTree;
using Project.AI.BehaviourTree.Nodes;

[CustomEditor(typeof(CompositeNode), true)]
public class CompositeNodeEditor : Editor
{
    private CompositeNode m_Node;



    private void OnEnable()
    {
        m_Node = target as CompositeNode;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.BeginHorizontal();

        if (GUILayout.Button("Add Port"))
        {
            m_Node.AddNodePort();
            EditorWindow.GetWindow<BehaviourTreeEditorWindow>().InitEditor();
        }

        if(GUILayout.Button("Remove Port"))
        {
            m_Node.RemoveNodePort();
            EditorWindow.GetWindow<BehaviourTreeEditorWindow>().InitEditor();
        }

        EditorGUILayout.EndHorizontal();
    }
}
