using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Project.AI.BehaviourTree.Editors
{
    [CustomEditor(typeof(BehaviourTree)), InitializeOnLoad]
    public class BehaviourTreeEditor : Editor
    {
        public BehaviourTreeEditor()
        {
            EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;
            EditorApplication.update += EditorApplication_update;
        }

        private void EditorApplication_playModeStateChanged(PlayModeStateChange state)
        {
            //BehaviourTreeEditorWindow.OnPlayModeChanged(state);
        }

        private void EditorApplication_update()
        {
            //BehaviourTreeEditorWindow.OnEditorUpdate();
        }
    }
}