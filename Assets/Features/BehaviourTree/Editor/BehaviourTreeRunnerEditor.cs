using UnityEngine;
using UnityEditor;

namespace Project.AI.BehaviourTree.Editors
{
    [CustomEditor(typeof(BehaviourTreeRunner)), InitializeOnLoad]
    public class BehaviourTreeRunnerEditor : Editor
    {
        private BehaviourTreeRunner m_Runner;
        private BehaviourTreeEditorWindow m_EditorWindow;



        public BehaviourTreeRunnerEditor()
        {
            EditorApplication.update += EditorApplication_update;
        }

        private void OnEnable()
        {
            m_Runner = target as BehaviourTreeRunner;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if(GUILayout.Button("OPEN"))
            {
                m_Runner.onTreeStarted += OnTreeStarted;

                m_EditorWindow = BehaviourTreeEditorWindow.OpenWindow();

                if (m_Runner.CurrentTree != null)
                    m_EditorWindow.InitEditor(m_Runner.CurrentTree);
            }
        }

        private void OnTreeStarted(BehaviourTreeRunner runner)
        {
            m_EditorWindow.OnTreeStarted(runner);
        }

        private void EditorApplication_update()
        {
            if (Application.isPlaying)
            {
                if(m_EditorWindow != null)
                {
                    m_EditorWindow.OnEditorUpdate();
                }
            }
        }
    }
}