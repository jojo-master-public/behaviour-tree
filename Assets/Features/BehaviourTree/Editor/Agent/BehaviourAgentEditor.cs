using UnityEngine;
using UnityEditor;
using Project.AI.BehaviourTree.Agents;

namespace Project.AI.BehaviourTree.Editors
{
    [CustomEditor(typeof(BehaviourAgent))]
    public class BehaviourAgentEditor : Editor
    {
        private BehaviourAgent m_Agent;



        private void OnEnable()
        {
            m_Agent = target as BehaviourAgent;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.LabelField("Data");

            foreach (var kvp in m_Agent.Data)
            {
                EditorGUILayout.BeginHorizontal();

                EditorGUILayout.PrefixLabel(kvp.Key);
                EditorGUILayout.LabelField(kvp.Value.ToString());

                EditorGUILayout.EndHorizontal();
            }
        }
    }
}