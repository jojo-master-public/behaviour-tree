using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public enum PropertyDisplayType
{
    Labeled,
    NonLabeled
}

public abstract class DropDownPropertyDrawer : PropertyDrawer
{
    protected abstract string m_PropertyName { get; }
    protected abstract PropertyDisplayType m_DisplayType { get; }



    public class IdArgument
    {
        public SerializedProperty Property;
        public string Id;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        var idProp = property.FindPropertyRelative(m_PropertyName);
        var displayPosition = position;

        if(m_DisplayType == PropertyDisplayType.Labeled)
        {
            displayPosition = EditorGUI.PrefixLabel(position, new GUIContent(label));
        }

        if (EditorGUI.DropdownButton(displayPosition, new GUIContent(idProp.stringValue), FocusType.Keyboard))
        {
            DisplayItemsMenu(idProp);
        }
    }

    protected abstract IEnumerable<string> LoadItemIds(SerializedProperty property);

    protected virtual void DisplayItemsMenu(SerializedProperty property)
    {
        var items = LoadItemIds(property);

        GenericMenu menu = new GenericMenu();

        var currentId = property.stringValue;

        {
            var itemIdArg = new IdArgument()
            {
                Property = property,
                Id = string.Empty
            };

            menu.AddItem(new GUIContent("Empty String"), string.Equals(currentId, string.Empty), OnItemSelected, itemIdArg);
        }

        foreach (var item in items)
        {
            var itemIdArg = new IdArgument()
            {
                Property = property,
                Id = item
            };

            menu.AddItem(new GUIContent(item), string.Equals(currentId, item), OnItemSelected, itemIdArg);
        }

        menu.ShowAsContext();
    }

    protected virtual void OnItemSelected(object item)
    {
        if (item == null)
            return;

        var itemIdProp = (IdArgument)item;

        itemIdProp.Property.stringValue = itemIdProp.Id;
        itemIdProp.Property.serializedObject.ApplyModifiedProperties();
    }
}