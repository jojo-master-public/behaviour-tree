using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class InputActionsLoader : MonoBehaviour
{
    [SerializeField] private InputActionAsset m_ActionsAsset;



    private void OnEnable()
    {
        m_ActionsAsset.Enable();
    }

    private void OnDisable()
    {
        m_ActionsAsset.Disable();
    }
}
