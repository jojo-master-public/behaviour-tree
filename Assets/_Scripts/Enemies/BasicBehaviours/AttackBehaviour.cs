using System;
using UnityEngine;

public abstract class AttackBehaviour : MonoBehaviour
{
    public event Action onAttackFinished;



    public abstract void Execute();
}
