using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Enemies.Behaviours
{
    public abstract class EnemyBehaviourBase : MonoBehaviour
    {
        protected Enemy m_Enemy;



        public virtual void Init(Enemy target)
        {
            m_Enemy = target;
        }

        public virtual void StartBehaviour() { }
        public virtual void UpdateBehaviour(float deltaTime) { }
        public virtual void StopBehaviour() { }
    }
}