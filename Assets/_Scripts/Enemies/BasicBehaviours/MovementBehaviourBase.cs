using UnityEngine;

namespace Project.Enemies.Behaviours
{
    public abstract class MovementBehaviourBase : EnemyBehaviourBase
    {
        [SerializeField] protected float m_MovementSpeed;



        public abstract void MoveTo(Vector2 position);

        public void Stop()
        {
            m_Enemy.EnemyRB.velocity = Vector2.zero;
        }

        #region EDITOR

        [SerializeField] protected bool m_DebugMoveToPosition;

        #endregion
    }
}