using UnityEngine;

namespace Project.Enemies.Behaviours
{
    public class WalkBehaviour : MovementBehaviourBase
    {
        public override void MoveTo(Vector2 position)
        {
            var direction = Mathf.Sign(position.x - m_Enemy.Position.x);

            var velocity = m_Enemy.EnemyRB.velocity;

            velocity.x = direction * m_MovementSpeed;
            m_Enemy.EnemyRB.velocity = velocity;

#if UNITY_EDITOR

            if(m_DebugMoveToPosition)
            {
                UnityEngine.Debug.DrawLine(transform.position, position);
            }

#endif
        }
    }
}
