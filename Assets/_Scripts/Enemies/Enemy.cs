﻿using Project.Behaviours;
using Project.Enemies.Behaviours;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(HealthComponent))]
public class Enemy : MonoBehaviour, ITargettable
{
    public Vector2 Position => transform.position;
    public Vector2 Velocity => Position - m_LastPosition;
    public TargetPriority Priority => TargetPriority.ENEMY;
    public Rigidbody2D EnemyRB => m_RigidBody;
    public HealthComponent HealthComponent { get; private set; }
    public Transform m_Player { get; private set; }

    private Vector2 m_LastPosition;
    private Rigidbody2D m_RigidBody;



    private void Awake()
    {
        HealthComponent = GetComponent<HealthComponent>();
        HealthComponent.InitHealth();

        m_RigidBody = GetComponent<Rigidbody2D>();

        m_Player = GameObject.FindWithTag("Player")?.transform;

        var enemyComponents = GetComponents<EnemyBehaviourBase>();

        foreach (var ec in enemyComponents)
        {
            ec.Init(this);
        }
    }
    
    protected void Update()
    {
        m_LastPosition = Position;
    }
}