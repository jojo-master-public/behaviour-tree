using UnityEngine;

public class VisionComponent : MonoBehaviour
{
    public float VisionRange => m_VisionRange;

    [SerializeField] private float m_VisionRange;


    #region DEBUG

    [SerializeField] private bool m_Debug;

    private void OnDrawGizmos()
    {
        if (!m_Debug)
            return;

        Gizmos.color = Color.yellow;

        Gizmos.DrawWireSphere(transform.position, VisionRange);
    }

    #endregion
}
