using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalSuitablePositionCheck : SuitablePositionComponentBase
{
    private Transform m_Player;



    private void Awake()
    {
        var agent = GetComponent<Enemy>();
        m_Player = agent?.m_Player;
    }

    public override bool CheckSuitablePosition(Enemy agent)
    {
        var suitablePosition = GetSuitablePosition(agent);

        return Mathf.Abs(suitablePosition.x - agent.Position.x) < 0.1f;
    }

    public override Vector2 GetSuitablePosition(Enemy agent)
    {
        var playerPosition = m_Player.position;

        var directionFromPlayer = Mathf.Sign(agent.Position.x - playerPosition.x);

        var offset = Vector2.right * directionFromPlayer * SuitableDistance;

        return (Vector2)playerPosition + offset;
    }


    #region DEBUG

    [SerializeField] private bool m_Debug;

    private void OnDrawGizmos()
    {
        if (!m_Debug)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position + Vector3.right * SuitableDistance, Vector3.up * 5);
        Gizmos.DrawRay(transform.position - Vector3.right * SuitableDistance, Vector3.up * 5);
    }

    #endregion
}
