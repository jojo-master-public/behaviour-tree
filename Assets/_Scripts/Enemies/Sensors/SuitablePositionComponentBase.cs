using UnityEngine;

public abstract class SuitablePositionComponentBase : MonoBehaviour
{
    [SerializeField] private float m_SuitableDistance;

    public float SuitableDistance => m_SuitableDistance;



    public abstract bool CheckSuitablePosition(Enemy agent);
    public abstract Vector2 GetSuitablePosition(Enemy agent);
}
