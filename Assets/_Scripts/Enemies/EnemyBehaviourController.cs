using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Enemies.Behaviours
{
    public class EnemyBehaviourController : MonoBehaviour
    {
        private EnemyBehaviourBase[] m_Behaviours;



        private void Start()
        {
            var enemy = GetComponent<Enemy>();

            m_Behaviours = GetComponents<EnemyBehaviourBase>();

            foreach (var beh in m_Behaviours)
            {
                beh.Init(enemy);
                beh.StartBehaviour();
            }
        }

        private void Update()
        {
            float deltaTime = Time.deltaTime;

            foreach (var beh in m_Behaviours)
            {
                if(beh.enabled)
                    beh.UpdateBehaviour(deltaTime);
            }
        }

        private void OnDisable()
        {
            foreach (var beh in m_Behaviours)
            {
                beh.StopBehaviour();
            }
        }
    }
}