﻿using System;
using UnityEngine;

namespace Project.Physics
{
    public delegate void CollisionStateChanged(bool state);
    
    /// <summary> Just checks for ground and wall collisions and sends events accordingly </summary>
    public class PlayerCollision : MonoBehaviour
    {
        public event CollisionStateChanged onGroundStateChanged;
        public event CollisionStateChanged onLeftWallStateChanged;
        public event CollisionStateChanged onRightWallStateChanged;

        [SerializeField] private float m_GroundCheckRadius;
        [SerializeField] private Vector2 m_GroundCheckOffset;

        [SerializeField] private float m_WallCheckRadius;
        [SerializeField] private Vector2 m_WallCheckOffset;

        [SerializeField] private LayerMask m_CollisionMask;

        private bool m_OnGround;
        private bool m_OnLeftWall;
        private bool m_OnRightWall;
        private Collider2D[] m_CollisionBuffer = new Collider2D[1];



        private void FixedUpdate()
        {
            Vector2 playerPosition = transform.position;

            CheckCollision(playerPosition + m_GroundCheckOffset, m_GroundCheckRadius, ref m_OnGround, onGroundStateChanged);

            Vector2 leftWallOffset = new Vector2(-m_WallCheckOffset.x, m_WallCheckOffset.y);

            CheckCollision(playerPosition + leftWallOffset, m_WallCheckRadius, ref m_OnLeftWall, onLeftWallStateChanged);
            CheckCollision(playerPosition + m_WallCheckOffset, m_WallCheckRadius, ref m_OnRightWall, onRightWallStateChanged);
        }

        private void CheckCollision(Vector2 checkPos, float checkRadius, ref bool outputBool, CollisionStateChanged onStateChangedCallback)
        {
            ClearBuffer();

            Physics2D.OverlapCircleNonAlloc(checkPos, checkRadius, m_CollisionBuffer, m_CollisionMask);

            bool isColliding = m_CollisionBuffer[0] != null;

            if (isColliding != outputBool)
                onStateChangedCallback?.Invoke(isColliding);

            outputBool = isColliding;
        }

        private void ClearBuffer()
        {
            m_CollisionBuffer[0] = null;
        }

        private void ChopChop()
        {
            int chop_chop = 2;

            ComonComon(chop_chop);
            
            int ComonComon(int hello)
            {
                return hello * 2;
            }
        }

#if UNITY_EDITOR

        [SerializeField] private bool m_Debug;

        private void OnDrawGizmos()
        {
            if (!m_Debug)
                return;

            Gizmos.color = Color.magenta;

            Gizmos.DrawWireSphere((Vector2)transform.position + m_GroundCheckOffset, m_GroundCheckRadius);
            Gizmos.DrawWireSphere((Vector2)transform.position + m_WallCheckOffset, m_WallCheckRadius);
            Gizmos.DrawWireSphere((Vector2)transform.position + new Vector2(-m_WallCheckOffset.x, m_WallCheckOffset.y), m_WallCheckRadius);
        }

#endif
    }
}