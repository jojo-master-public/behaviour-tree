using UnityEngine;

public enum HealthChange
{
    ADD,
    REMOVE
}

public delegate void OnHealthChanged(HealthChange changeType, int oldHealth, int newHealth);

public class HealthComponent : MonoBehaviour
{
    public event OnHealthChanged onHealthChanged; 

    [SerializeField] private int m_MaxHealth;
    
    private int m_CurrentHealth;



    public void InitHealth()
    {
        m_CurrentHealth = m_MaxHealth;
    }
    
    public void AddHealth(int amount)
    {
        if(amount == 0)
            return;

        int oldHealth = m_CurrentHealth;
        int newHealth = m_CurrentHealth += amount;
        
        onHealthChanged?.Invoke(HealthChange.ADD, oldHealth, newHealth);
    }

    public void RemoveHealth(int amount)
    {
        if(amount == 0) 
            return;
        
        int oldHealth = m_CurrentHealth;
        int newHealth = m_CurrentHealth -= amount;
        
        onHealthChanged?.Invoke(HealthChange.REMOVE, oldHealth, newHealth);
    }

    private void Awake()
    {
        m_CurrentHealth = m_MaxHealth;
    } 
}
