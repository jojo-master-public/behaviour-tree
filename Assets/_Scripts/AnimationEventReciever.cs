﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AnimationEventReciever : MonoBehaviour
{
    public event Action<bool> onScaleStateChanged;
    public UnityEvent onStart, onFinish;



    public void OnAnimationStart()
    {
        onStart?.Invoke();
    }

    public void OnAnimationFinish()
    {
        onFinish?.Invoke();
    }

    public void SetScaleState(int state)
    {
        onScaleStateChanged?.Invoke(state == 1);
    }
}
