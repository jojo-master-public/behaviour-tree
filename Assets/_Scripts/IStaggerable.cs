using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Project.Behaviours
{
    public interface IStaggerable
    {
        void Stagger();
    }
}