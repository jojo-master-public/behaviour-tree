using UnityEngine;

public enum TargetPriority
{
    INTERACTION,
    ENEMY
}

public interface ITargettable
{
    Vector2 Position { get; }
    Vector2 Velocity { get; }
    TargetPriority Priority { get; }
}